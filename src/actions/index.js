export const addData = data => ({
  type: 'ADD_DATA',
  payload: data,
});
export const updateData = (data, id) => ({
  type: 'DELETE_DATA',
  payload: {
    id,
    data,
  },
});
export const filteredData = data => ({
  type: 'FILTERED_DATA',
  payload: data,
});
