import React from 'react';
import {StyleSheet, Text, TextInput, View} from 'react-native';

const Input = props => {
  return (
    <View style={styles.inputText}>
      <TextInput
        keyboardType={props.keyboardType ?? 'default'}
        style={styles.input}
        {...props}
      />
      {props.error && <Text style={styles.text}>{props.error}</Text>}
    </View>
  );
};
const styles = StyleSheet.create({
  inputText: {marginBottom: 22},
  input: {
    height: 40,
    borderWidth: 1,
    padding: 10,
    width: 300,
  },
  text: {
    color: 'red',
  },
});
export default Input;
