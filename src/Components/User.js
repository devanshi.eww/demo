import React, {useState} from 'react';
import {Pressable, StyleSheet, Text, View} from 'react-native';
import {Icon} from 'react-native-elements';

const User = props => {
  const [checked, setChecked] = useState('check-box-outline-blank');

  const onClick = () => {
    props.onChangeHandler(props.user);
  };

  const clicked = () => {
    if (checked === 'check-box') {
      setChecked('check-box-outline-blank');
    } else {
      setChecked('check-box');
    }
  };

  // const deleteItems = () => {
  //   if (checked === 'check-box') {
  //     props.onChangeHandler(props.user);
  //   }
  // };

  const styles = StyleSheet.create({
    text: {
      fontSize: 18,
      color: 'whitesmoke',
    },
  });
  return (
    <>
      <View
        style={{
          display: 'flex',
          flexDirection: 'row',
          padding: 10,
          backgroundColor: 'gray',
          marginBottom: 10,
        }}>
        <View style={{flex: 1}}>
          <Text style={styles.text}>Name : {props.user?.name}</Text>
          <Text style={styles.text}>Mail-ID : {props.user?.mail}</Text>
          <Text style={styles.text}>Phone Number : {props.user?.tel}</Text>
        </View>
        <Pressable onPress={onClick} style={{margin: 5}}>
          <Icon name="delete" color="white" />
        </Pressable>

        <Pressable style={{margin: 5}} onPress={clicked}>
          <Icon name={checked} color="white" />
        </Pressable>
      </View>
      {/* <Pressable
        onPress={deleteItems}
        style={{
          padding: 10,
          backgroundColor: 'red',
          marginBottom: 10,
        }}>
        <Text
          style={{
            textAlign: 'center',
            color: 'whitesmoke',
            fontWeight: 'bold',
            fontSize: 16,
          }}>
          Delete All Selected Items
        </Text>
      </Pressable> */}
    </>
  );
};

export default User;
