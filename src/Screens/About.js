import React, {useState} from 'react';
import {StyleSheet, View, Button, Text, Pressable} from 'react-native';
import Geolocation from '@react-native-community/geolocation';
// import GetLocation from 'react-native-get-location';
// import Geocoder from 'react-native-geocoder';

const About = ({navigation}) => {
  // const requestLocation = () =>
  //   GetLocation.getCurrentPosition({
  //     enableHighAccuracy: true,
  //     timeout: 15000,
  //   })
  //     .then(location => {
  //       console.log(location);
  //       fetch(
  //         'https://maps.googleapis.com/maps/api/geocode/json?address=' +
  //           location.latitude +
  //           ',' +
  //           location.longitude +
  //           '&key=' +
  //           'AIzaSyBVNbt_6Gpxn5YZm70LcbUMW4VLKrtuUWg',
  //       )
  //         .then(response => response.json())
  //         .then(responseJson => {
  //           console.log(
  //             'ADDRESS GEOCODE is BACK!! => ' + JSON.stringify(responseJson),
  //           );
  //         });
  //     })
  //     .catch(error => {
  //       const {code, message} = error;
  //       console.warn(code, message);
  //     });
  const [error, setError] = useState('');
  const [position, setPosition] = useState({
    latitude: 0,
    longitude: 0,
  });

  const requestLocation = () => {
    // GeolocationPosition.name(name => console.log(name, 'name place'));

    Geolocation.getCurrentPosition(
      pos => {
        console.log(pos, 'position');
        setError('');
        setPosition({
          latitude: pos.coords.latitude,
          longitude: pos.coords.longitude,
        });
      },
      e => e.message,
    );
    Geolocation.watchPosition(pos => console.log(pos, 'position'));
  };
  return (
    <View
      contentContainerStyle={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <View style={styles.centeredView}>
        <Text>Already have an account?</Text>
        <Button
          onPress={() => navigation.navigate('Login')}
          title="Log In"
          color="#841584"
          accessibilityLabel="Learn more about this purple button"
        />
        <Pressable
          onPress={requestLocation}
          style={{
            padding: 10,
            borderWidth: 1,
            borderRadius: 7,
            marginTop: 10,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text>Get Position</Text>
        </Pressable>

        {error ? (
          <Text>Error retrieving current position</Text>
        ) : (
          <>
            <Text>Latitude: {position.latitude}</Text>
            <Text>Longitude: {position.longitude}</Text>
          </>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  centeredView: {
    marginTop: 50,
  },
});

export default About;
