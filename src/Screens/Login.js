import React, {useState} from 'react';
import {StyleSheet, View, Button, Pressable, Text} from 'react-native';
import {useDispatch} from 'react-redux';
import {addData} from '../actions';
import Input from '../Components/Input';
import {validMail, validName, validPhone} from '../validate';
// import {
//   GoogleSignin,
//   statusCodes,
// } from '@react-native-google-signin/google-signin';
// import auth from '@react-native-firebase/auth';
// import {LoginManager, AccessToken} from 'react-native-fbsdk-next';
// import appleAuth, {
//   AppleButton,
// } from '@invertase/react-native-apple-authentication';
// import {Alert} from 'react-native';

// async function onFacebookButtonPress() {
//   // Attempt login with permissions
//   const result = await LoginManager.logInWithPermissions(['public_profile']);
//   console.log(result, 'result');

//   if (result.isCancelled) {
//     throw 'User cancelled the login process';
//   }

//   // Once signed in, get the users AccesToken
//   const data = await AccessToken.getCurrentAccessToken();

//   if (!data) {
//     throw 'Something went wrong obtaining access token';
//   }

//   // Create a Firebase credential with the AccessToken
//   const facebookCredential = auth.FacebookAuthProvider.credential(
//     data.accessToken,
//   );

//   // Sign-in the user with the credential
//   return auth().signInWithCredential(facebookCredential);
// }
// const onFacebookButtonPress = async () => {
//   try {
//     // Login the User and get his public profile and email id.
//     const result = await LoginManager.logInWithPermissions([
//       'public_profile',
//       'email',
//     ]);

//     // If the user cancels the login process, the result will have a
//     // isCancelled boolean set to true. We can use that to break out of this function.
//     if (result.isCancelled) {
//       throw 'User cancelled the login process';
//     }

//     // Get the Access Token
//     const data = await AccessToken.getCurrentAccessToken();

//     // If we don't get the access token, then something has went wrong.
//     if (!data) {
//       throw 'Something went wrong obtaining access token';
//     }

//     // Use the Access Token to create a facebook credential.
//     const facebookCredential = auth.FacebookAuthProvider.credential(
//       data.accessToken,
//     );

//     // Use the facebook credential to sign in to the application.
//     return auth().signInWithCredential(facebookCredential);
//   } catch (error) {
//     alert(error);
//   }
// };
const Login = () => {
  //   GoogleSignin.configure();
  //   const [state, setState] = useState({});

  //   const signIn = async () => {
  //     try {
  //       await GoogleSignin.hasPlayServices();
  //       const userInfo = await GoogleSignin.signIn();
  //       setState(userInfo);
  //     } catch (error) {
  //       if (error.code === statusCodes.SIGN_IN_REQUIRED) {
  //         Alert.alert('Please Sign in');
  //         setState({isLoggedIn: false});
  //       } else {
  //         Alert.alert('Something else went wrong... ', error.toString());
  //         setState({isLoggedIn: false});
  //       }
  //     }
  //   };

  const [name, setName] = useState('');
  const [mail, setMail] = useState('');
  const [tel, setTel] = useState('');
  const dispatch = useDispatch();
  const data = {name, mail, tel};
  const [valid, setValid] = useState(true);
  const handleSubmit = () => {
    if (
      validName(name) !== true ||
      validMail(mail) !== true ||
      validPhone(tel) !== true
    ) {
      return setValid(false);
    }
    return setValid(true);
  };

  const onClick = id => {
    handleSubmit();
    if (
      validName(name) === true &&
      validMail(mail) === true &&
      validPhone(tel) === true
    ) {
      dispatch(addData(data));
      setName('');
      setMail('');
      setTel('');
      setValid(true);
    }
  };

  const styles = StyleSheet.create({
    centeredView: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: 22,
    },
  });

  //   async function onAppleButtonPress() {
  //     // performs login request
  //     const appleAuthRequestResponse = await appleAuth.performRequest({
  //       requestedOperation: appleAuth.Operation.LOGIN,
  //       requestedScopes: [appleAuth.Scope.EMAIL, appleAuth.Scope.FULL_NAME],
  //     });
  //     console.log('user nottttt auth');

  //     // get current authentication state for user
  //     // /!\ This method must be tested on a real device. On the iOS simulator it always throws an error.
  //     const credentialState = await appleAuth.getCredentialStateForUser(
  //       appleAuthRequestResponse.user,
  //     );

  //     // use credentialState response to ensure the user is authenticated
  //     if (credentialState === appleAuth.State.AUTHORIZED) {
  //       console.log('user authenticated');
  //       // user is authenticated
  //     }
  //   }

  return (
    <View style={styles.centeredView}>
      <Input
        name="name"
        placeholder="Username"
        defaultValue={name}
        onChangeText={setName}
        error={!valid ? validName(name) : null}
        onChange={() => setValid(true)}
      />
      <Input
        name="mail"
        keyboardType="email-address"
        placeholder="E-mail ID"
        defaultValue={mail}
        onChangeText={setMail}
        error={!valid ? validMail(mail) : null}
        onChange={() => setValid(true)}
      />
      <Input
        name="tel"
        keyboardType="phone-pad"
        placeholder="Mobile Number"
        defaultValue={tel}
        onChangeText={setTel}
        error={!valid ? validPhone(tel) : null}
        onChange={() => setValid(true)}
      />
      <Button
        onPress={onClick}
        title="Log In/Sign In"
        color="#841584"
        accessibilityLabel="Learn more about this purple button"
      />
      <Pressable
        style={{
          paddingHorizontal: 10,
          paddingVertical: 5,
          borderColor: 'black',
          borderWidth: 1,
          borderRadius: 5,
          marginTop: 20,
        }}
        // onPress={() =>
        //   signIn().then(() => console.log('Signed in with Google!'))
        // }
      >
        <Text>Google</Text>
      </Pressable>
      <Pressable
        style={{
          paddingHorizontal: 10,
          paddingVertical: 5,
          borderColor: 'black',
          borderWidth: 1,
          borderRadius: 5,
          marginTop: 20,
        }}
        // onPress={() =>
        //   onFacebookButtonPress().then(() =>
        //     console.log('Signed in with Facebook!'),
        //   )
        // }
      >
        <Text>Facebook</Text>
      </Pressable>
      {/* <AppleButton
        buttonStyle={AppleButton.Style.WHITE}
        buttonType={AppleButton.Type.SIGN_IN}
        style={{
          width: 150,
          height: 45,
        }}
        // onPress={() => onAppleButtonPress()}
      /> */}
    </View>
  );
};

export default Login;
