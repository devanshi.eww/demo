import React, {useEffect, useState} from 'react';
import BleManager from 'react-native-ble-manager';
import {
  Text,
  ScrollView,
  View,
  Alert,
  Pressable,
  NativeEventEmitter,
  NativeModules,
  NativeAppEventEmitter,
} from 'react-native';

const Home = ({navigation}) => {
  // const BleManagerModule = NativeModules.BleManager;

  // const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);
  // const [devices, setDevices] = useState([]);

  // useEffect(() => {
  //   BleManager.start({showAlert: false}).then(() => {
  //     // Success code
  //     console.log('Module initialized');
  //   });
  // }, []);
  // const connectToDevice = async id => {
  //   console.log('pressed', id);
  //   bleManagerEmitter.addListener(
  //     'BleManagerDidUpdateValueForCharacteristic',
  //     ({value, characteristic}) => {
  //       function bin2String(array) {
  //         var result = '';
  //         for (var i = 0; i < array.length; i++) {
  //           result += String.fromCharCode(parseInt(array[i]));
  //         }
  //         return result;
  //       }
  //       // Convert bytes array to string
  //       console.log('event listner');
  //       // const data = bytesToString(value);
  //       // navigation.navigate('About', bin2String(value));
  //       console.log(value);
  //       console.log(
  //         `Recieved ${bin2String(value)} for characteristic ${characteristic}`,
  //       );
  //     },
  //   );
  //   bleManagerEmitter.addListener(
  //     'handleUpdateValueForCharacteristic',
  //     ({value, characteristic}) => {
  //       function bin2String(array) {
  //         var result = '';
  //         for (var i = 0; i < array.length; i++) {
  //           result += String.fromCharCode(parseInt(array[i]));
  //         }
  //         return result;
  //       }
  //       // Convert bytes array to string
  //       console.log('handleUpdateValueForCharacteristic : ');
  //       // const data = bytesToString(value);
  //       // navigation.navigate('About', bin2String(value));
  //       console.log(
  //         `Recieved ${bin2String(value)} for characteristic ${characteristic}`,
  //       );
  //     },
  //   );
  //   // BleManager.createBond(id)
  //   //   .then(() => {
  //   //     Alert.alert(
  //   //       'Success!!',
  //   //       'createBond success or there is already an existing one',
  //   //     );
  //   //   })
  //   //   .catch(error => {
  //   //     Alert.alert('Request fail', error);
  //   //   });
  //   await BleManager.connect(id)
  //     .then(() => {
  //       // Success code

  //       BleManager.retrieveServices(id).then(peripheralInfo => {
  //         console.log('Peripheral info:', peripheralInfo);
  //         console.log('Peripheral id:', peripheralInfo.id);
  //         BleManager.stopScan();
  //         BleManager.startNotification(
  //           id,
  //           peripheralInfo.characteristics[4].service,
  //           peripheralInfo.characteristics[4].characteristic,
  //         )
  //           .then(data => {
  //             // Success code

  //             console.log('Notification started');
  //             return NativeAppEventEmitter.addListener(
  //               'BleManagerDidUpdateValueForCharacteristic',
  //               data => {
  //                 console.log('notification updated value', data.toString());
  //               },
  //             );
  //           })
  //           .catch(error => {
  //             // Failure code
  //             console.log(error);
  //           });

  //         //   const data = [82, 120, 95, 99, 104, 101, 99, 107, 10];
  //         function convertStringToByteArray(str) {
  //           String.prototype.encodeHex = function () {
  //             var bytes = [];
  //             for (var i = 0; i < this.length; ++i) {
  //               bytes.push(this.charCodeAt(i));
  //             }
  //             return bytes;
  //           };

  //           var byteArray = str.encodeHex();
  //           return byteArray;
  //         }
  //         const data = convertStringToByteArray('hello;;;;;');
  //         BleManager.write(
  //           id,
  //           peripheralInfo.characteristics[5].service,
  //           peripheralInfo.characteristics[5].characteristic,
  //           data,
  //         )
  //           .then(() => {
  //             console.log(data, 'write data');
  //           })
  //           .catch(err => {
  //             console.log(err, 'write is failed');
  //           });

  //         function string2Bin(str) {
  //           var result = [];
  //           for (var i = 0; i < str.length; i++) {
  //             result.push(str.charCodeAt(i).toString(2));
  //           }
  //           return result;
  //         }

  // BleManager.read(
  //   id,
  //   peripheralInfo.characteristics[4].service,
  //   peripheralInfo.characteristics[4].characteristic,
  // )
  //   .then(readData => {
  //     // Success code
  //     // console.log('Read: ' + readData);

  //     const buffer = Buffer.Buffer.from(readData); //https://github.com/feross/buffer#convert-arraybuffer-to-buffer
  //     const sensorData = buffer.readUInt8(1, true);
  //     console.log(sensorData, 'sensor data');
  //   })
  //   .catch(error => {
  //     // Failure code
  //     console.log('read error', error);
  //   });
  // read(
  //   id,
  //   '00001801-0000-1000-8000-00805f9b34fb',
  //   '6E400002-B5A3-F393-E0A9-E50E24DCCA9E',
  // ).then(value => {
  //   console.log(value, 'value;;;;');
  // });
  // BleManager.startNotification(
  //   id,
  //   '6E400001-B5A3-F393-E0A9-E50E24DCCA9E',
  //   '6E400002-B5A3-F393-E0A9-E50E24DCCA9E',
  // )
  //   .then(() => {
  //     // Success code
  //     console.log('Notification started');
  //   })
  //   .catch(error => {
  //     // Failure code
  //     console.log(error);
  //   });
  // Success code
  //       });
  //       Alert.alert('Connected', 'bluetooth connected');
  //     })
  //     .catch(error => {
  //       // Failure code
  //       Alert.alert('error', error);
  //     });
  // };
  // const disconnect = id => {
  //   BleManager.disconnect(id)
  //     .then(() => {
  //       // Success code
  //       Alert.alert('Device Disconnected', id);
  //     })
  //     .catch(error => {
  //       // Failure code
  //       alert(error);
  //     });
  // BleManager.removeBond(id)
  //   .then(() => {
  //     alert('removeBond success');
  //   })
  //   .catch(() => {
  //     alert('fail to remove the bond');
  //   });
  // };
  // const enable = () => {
  //   BleManager.enableBluetooth()
  //     .then(() => {
  //       // Success code
  //       console.log('The bluetooth is already enabled or the user confirm');
  //       BleManager.scan([], 5, true).then(() => {
  //         // Success code
  //         console.log('Scan started');
  //         setTimeout(() => {
  //           BleManager.getDiscoveredPeripherals([]).then(peripheralsArray => {
  //             // Success code
  //             console.log('Discovered peripherals: ' + peripheralsArray.length);
  //             setDevices(peripheralsArray);
  //             console.log(peripheralsArray, 'Array');
  //           });
  //         }, 5000);
  //       });
  //     })
  //     .catch(error => {
  //       // Failure code
  //       console.log('The user refuse to enable bluetooth');
  //     });
  // };

  return (
    <ScrollView style={{backgroundColor: '#222', flex: 1}}>
      <Pressable
        style={{
          backgroundColor: 'red',
          flex: 1,
          padding: 9,
          justifyContent: 'center',
          alignItems: 'center',
        }}
        // onPress={enable}
        onPress={() => navigation.navigate('Details')}>
        <Text style={{color: '#fff'}}>start scan</Text>
      </Pressable>
      <Text style={{color: '#fff'}}>
        {/* Discovered peripherals: {devices.length} */}
        Discovered peripherals
      </Text>

      {/* {devices.map(x => {
        return (
          <View key={x.id}>
            <Text style={{color: '#fff'}}>
              this is {x.name} device and id {x.id}
            </Text>

            <Pressable
              style={{
                backgroundColor: 'green',
                flex: 1,
                padding: 9,
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={() => connectToDevice(x.id)}>
              <Text style={{color: '#fff'}}>connect {x.name}</Text>
            </Pressable>
            <Pressable
              style={{
                backgroundColor: 'red',
                flex: 1,
                padding: 9,
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={() => disconnect(x.id)}>
              <Text style={{color: '#fff'}}>disconnect {x.name}</Text>
            </Pressable>
          </View>
        );
      })} */}
    </ScrollView>
  );
};

export default Home;

// import React, {useState, useEffect} from 'react';
// import {View, Text, FlatList, Button, PermissionsAndroid} from 'react-native';
// import {BleManager} from 'react-native-ble-plx';

// export const manager = new BleManager();

// const requestPermission = async () => {
//   const granted = await PermissionsAndroid.request(
//     PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
//     {
//       title: 'Request for Location Permission',
//       message: 'Bluetooth Scanner requires access to Fine Location Permission',
//       buttonNeutral: 'Ask Me Later',
//       buttonNegative: 'Cancel',
//       buttonPositive: 'OK',
//     },
//   );
//   return granted === PermissionsAndroid.RESULTS.GRANTED;
// };

// // BlueetoothScanner does:
// // - access/enable bluetooth module
// // - scan bluetooth devices in the area
// // - list the scanned devices
// const BluetoothScanner = () => {
//   const [logData, setLogData] = useState([]);
//   const [logCount, setLogCount] = useState(0);
//   const [scannedDevices, setScannedDevices] = useState({});
//   const [deviceCount, setDeviceCount] = useState(0);

//   useEffect(() => {
//     manager.onStateChange(state => {
//       const subscription = manager.onStateChange(async state => {
//         console.log(state);
//         const newLogData = logData;
//         newLogData.push(state);
//         await setLogCount(newLogData.length);
//         await setLogData(newLogData);
//         subscription.remove();
//       }, true);
//       return () => subscription.remove();
//     });
//   }, [manager]);

//   return (
//     <View style={{flex: 1, padding: 10}}>
//       <View style={{flex: 1, padding: 10}}>
//         <Text style={{fontWeight: 'bold'}}>Bluetooth Log ({logCount})</Text>
//         <FlatList
//           data={logData}
//           renderItem={({item}) => {
//             return <Text>{item}</Text>;
//           }}
//         />
//         <Button
//           title="Turn On Bluetooth"
//           onPress={async () => {
//             const btState = await manager.state();
//             // test is bluetooth is supported
//             if (btState === 'Unsupported') {
//               alert('Bluetooth is not supported');
//               return false;
//             }
//             // enable if it is not powered on
//             if (btState !== 'PoweredOn') {
//               await manager.enable();
//             } else {
//               await manager.disable();
//             }
//             return true;
//           }}
//         />
//       </View>

//       <View style={{flex: 2, padding: 10}}>
//         <Text style={{fontWeight: 'bold'}}>
//           Scanned Devices ({deviceCount})
//         </Text>
//         <FlatList
//           data={Object.values(scannedDevices)}
//           renderItem={({item}) => {
//             return <Text>{`${item.name} (${item.id})`}</Text>;
//           }}
//         />
//         <Button
//           title="Scan Devices"
//           onPress={async () => {
//             const btState = await manager.state();
//             // test if bluetooth is powered on
//             if (btState !== 'PoweredOn') {
//               alert('Bluetooth is not powered on');
//               return false;
//             }
//             // explicitly ask for user's permission
//             const permission = await requestPermission();
//             if (permission) {
//               manager.startDeviceScan(null, null, async (error, device) => {
//                 // error handling
//                 if (error) {
//                   console.log(error);
//                   return;
//                 }
//                 // found a bluetooth device
//                 if (device) {
//                   console.log(`${device.name} (${device.id})}`);
//                   const newScannedDevices = scannedDevices;
//                   newScannedDevices[device.id] = device;
//                   await setDeviceCount(Object.keys(newScannedDevices).length);
//                   await setScannedDevices(scannedDevices);
//                 }
//               });
//             }
//             return true;
//           }}
//         />
//       </View>
//     </View>
//   );
// };

// export default BluetoothScanner;
