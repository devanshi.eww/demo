import React, {useState} from 'react';
import {Button, Switch, Text, View, FlatList, Pressable} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import User from '../Components/User';
import {updateData} from '../actions/index';

const Details = ({navigation}) => {
  const data = useSelector(state => state);

  const dispatch = useDispatch();
  const [isEnabled, setIsEnabled] = useState(false);

  const toggleSwitch = () => setIsEnabled(previousState => !previousState);
  return (
    <View
      style={{
        backgroundColor: isEnabled ? '#222' : '#fff',
        flex: 1,
        padding: 10,
      }}>
      <Switch
        trackColor={{false: '#222', true: '#fff'}}
        thumbColor={isEnabled ? 'whitesmoke' : 'gray'}
        ios_backgroundColor="#3e3e3e"
        onValueChange={toggleSwitch}
        value={isEnabled}
      />
      <Text
        style={{
          fontWeight: 'bold',
          fontSize: 22,
          color: isEnabled ? '#fff' : '#222',
          marginBottom: 10,
        }}>
        User Details
      </Text>
      <FlatList
        data={data?.addData}
        renderItem={({item, index}) => {
          return (
            <User
              user={item}
              index={index}
              onChangeHandler={item => {
                dispatch(updateData(item, index));
              }}
            />
          );
        }}
      />
      <Pressable
        onPress={() => {}}
        style={{
          padding: 10,
          backgroundColor: 'red',
          marginBottom: 10,
        }}>
        <Text
          style={{
            textAlign: 'center',
            color: 'whitesmoke',
            fontWeight: 'bold',
            fontSize: 16,
          }}>
          Delete All Selected Items
        </Text>
      </Pressable>
      <Button
        onPress={() => {
          navigation.goBack();
        }}
        color="#841584"
        title="Go Back To Home"
      />
    </View>
  );
};

export default Details;
