import React, {useState} from 'react';
import {Button, StyleSheet, View} from 'react-native';
import Input from '../Components/Input';

const Register = ({navigation, route}) => {
  const [name, setName] = useState(route.params?.name);
  const [mail, setMail] = useState(route.params?.mail);
  const [tel, setTel] = useState(route.params?.tel);
  return (
    <View style={styles.centeredView}>
      <Input
        name="name"
        placeholder="Username"
        defaultValue={name}
        onChangeText={setName}
      />
      <Input
        name="mail"
        keyboardType='email-address'
        placeholder="E-mail ID"
        defaultValue={mail}
        onChangeText={setMail}
      />
      <Input
        name="tel"
        keyboardType='phone-pad'
        placeholder="Mobile Number"
        defaultValue={tel}
        onChangeText={setTel}
      />
      <Button
        title="Submit"
        onPress={() => {
          navigation.replace('Login', {name, mail, tel});
        }}
        color="#841584"
        accessibilityLabel="Learn more about this purple button"
      />
    </View>
  );
};
const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
});
export default Register;
