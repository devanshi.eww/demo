export const validName = name => {
  if (!name.trim()) {
    return 'Name is required';
  }
  const regName = /^[a-zA-Z][a-zA-Z\\s]+$/;
  if (!regName.test(name)) {
    return 'Please enter valid name';
  }
  return true;
};
export const validMail = mail => {
  if (!mail.trim()) {
    return 'Mail is required';
  }
  const regMail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  if (!regMail.test(mail)) {
    return 'Plaese enter valid mail id';
  }
  return true;
};
export const validPhone = phone => {
  if (!phone.trim()) {
    return 'Phone number is required';
  }
  const regPhone =
    /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;

  if (!regPhone.test(phone)) {
    return 'Please enter valid phone number';
  }
  return true;
};
