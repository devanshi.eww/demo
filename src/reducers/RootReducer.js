const initialstate = {
  addData: [],
  updateData: {},
};

export const rootReducer = (state = initialstate, action) => {
  switch (action.type) {
    case 'ADD_DATA':
      state = {
        ...state,
        addData: [...state.addData, action.payload],
      };
      return state;
    case 'FILTERED_DATA':
      state = {
        ...state,
        updateData: action.payload,
      };
      return state;

    case 'DELETE_DATA':
      return {
        ...state,
        addData: [
          ...state.addData.slice(0, action.payload.id),
          ...state.addData.slice(action.payload.id + 1),
        ],
      };

    default:
      return state;
  }
};
