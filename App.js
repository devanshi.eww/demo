import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createStackNavigator} from '@react-navigation/stack';
import Home from './src/Screens/Home';
import About from './src/Screens/About';
import {NavigationContainer} from '@react-navigation/native';
import Details from './src/Screens/Details';
import {Provider} from 'react-redux';
import Login from './src/Screens/Login';
import {PersistGate} from 'redux-persist/integration/react';
import {persistor, store} from './src/Store/store';

const BOTTOM_STACK = createBottomTabNavigator();
const HOME_STACK = createStackNavigator();
const ABOUT_STACK = createStackNavigator();
const BASE_STACK = createStackNavigator();

export const HOME_STACK_NAVIGATOR = () => {
  return (
    <HOME_STACK.Navigator
      initialRouteName={'HomeScreen'}
      screenOptions={{headerShown: false}}>
      <HOME_STACK.Screen name={'HomeScreen'} component={Home} />
    </HOME_STACK.Navigator>
  );
};
export const ABOUT_STACK_NAVIGATOR = () => {
  return (
    <ABOUT_STACK.Navigator
      initialRouteName={'AboutScreen'}
      screenOptions={{headerShown: false}}>
      <ABOUT_STACK.Screen name={'AboutScreen'} component={About} />
    </ABOUT_STACK.Navigator>
  );
};

export const BOTTOM_STACK_NAVIGATOR = () => {
  return (
    <BOTTOM_STACK.Navigator
      initialRouteName="Home"
      mode="modal"
      screenOptions={() => ({
        headerShown: false,
        tabBarActiveTintColor: 'tomato',
        tabBarInactiveTintColor: 'gray',
      })}>
      <BOTTOM_STACK.Screen name="Home" component={HOME_STACK_NAVIGATOR} />
      <BOTTOM_STACK.Screen name="About" component={ABOUT_STACK_NAVIGATOR} />
    </BOTTOM_STACK.Navigator>
  );
};

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <NavigationContainer>
          <BASE_STACK.Navigator
            initialRouteName={'Main'}
            screenOptions={{headerShown: false}}>
            <BASE_STACK.Screen name="Main" component={BOTTOM_STACK_NAVIGATOR} />
            <BASE_STACK.Screen name="Details" component={Details} />
            <BASE_STACK.Screen name="Login" component={Login} />
          </BASE_STACK.Navigator>
        </NavigationContainer>
      </PersistGate>
    </Provider>
  );
};

export default App;
